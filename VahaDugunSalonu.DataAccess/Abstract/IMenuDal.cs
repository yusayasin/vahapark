﻿using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Abstract
{
    public interface IMenuDal : IRepository<Menu>
    {
        //T GetById(int id);
        //T GetOne(Expression<Func<T, bool>> filter);
        //IQueryable<T> GetAll(Expression<Func<T, bool>> filter);
        //IEnumerable<T> GetAll();
        //void Create(T entity);
        //void Update(T entity);
        //void Delete(T entity);
        //bool CreateByResult(T entity);
        //bool UpdateByResult(T entity);
        //bool DeleteByResult(T entity);
    }
}
