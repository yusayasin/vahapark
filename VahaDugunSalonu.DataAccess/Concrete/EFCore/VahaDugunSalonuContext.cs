﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class VahaDugunSalonuContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=127.0.0.1;Database=VAHAPARK;User ID=yykucuk;Password=yykucuk;Trusted_Connection=True;");
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(@"Server=127.0.0.1;Database=VAHAPARK;User ID=yykucuk;Password=yykucuk;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }

        public DbSet<AboutUs> AboutUss { get; set; }
        public DbSet<ContactInfo> ContactInfos { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Salon> Salons { get; set; }
        public DbSet<SalonImage> SalonImages { get; set; }
        public DbSet<Service> Services { get; set; }
        //public DbSet<UserAccount> UserAccounts { get; set; }
    }
}
