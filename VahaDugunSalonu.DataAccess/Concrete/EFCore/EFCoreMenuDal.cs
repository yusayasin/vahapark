﻿using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreMenuDal : EFCoreGenericRepository<Menu, VahaDugunSalonuContext>, IMenuDal
    {
    }
}
