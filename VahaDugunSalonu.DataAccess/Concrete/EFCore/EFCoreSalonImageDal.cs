﻿using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreSalonImageDal : EFCoreGenericRepository<SalonImage,VahaDugunSalonuContext>, ISalonImageDal
    {
    }
}
