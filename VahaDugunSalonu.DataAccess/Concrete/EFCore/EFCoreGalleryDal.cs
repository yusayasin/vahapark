﻿using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreGalleryDal : EFCoreGenericRepository<Gallery, VahaDugunSalonuContext>, IGalleryDal
    {

    }
}
