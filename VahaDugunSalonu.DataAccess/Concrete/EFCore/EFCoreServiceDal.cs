﻿using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreServiceDal : EFCoreGenericRepository<Service, VahaDugunSalonuContext>, IServiceDal
    {
    }
}
