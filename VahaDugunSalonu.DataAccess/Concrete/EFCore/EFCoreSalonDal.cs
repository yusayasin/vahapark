﻿using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreSalonDal : EFCoreGenericRepository<Salon, VahaDugunSalonuContext>, ISalonDal
    {
    }
}
