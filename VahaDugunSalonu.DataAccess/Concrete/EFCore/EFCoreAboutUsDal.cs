﻿using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreAboutUsDal : EFCoreGenericRepository<AboutUs, VahaDugunSalonuContext>, IAboutUsDal
    {
    }
}
