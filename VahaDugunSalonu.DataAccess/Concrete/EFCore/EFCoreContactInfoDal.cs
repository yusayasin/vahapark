﻿using System.Linq;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreContactInfoDal : EFCoreGenericRepository<ContactInfo, VahaDugunSalonuContext>, IContactInfoDal
    {
        public ContactInfo GetFirstContactInfo()
        {
            using(var context = new VahaDugunSalonuContext())
            {
                return context.Set<ContactInfo>().FirstOrDefault();
            }
        }
    }
}
