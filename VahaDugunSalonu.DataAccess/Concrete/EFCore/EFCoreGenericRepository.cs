﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;

namespace VahaDugunSalonu.DataAccess.Concrete.EFCore
{
    public class EFCoreGenericRepository<T, TContext> : IRepository<T> where T : class where TContext : DbContext, new()
    {

        public IList<T> GetAllByFilter(Expression<Func<T, bool>> filter)
        {
            using (var context = new TContext())
            {
                return context.Set<T>().Where(filter).ToList();
            }
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> filter)
        {
            using(var context = new TContext())
            {
                return context.Set<T>().Where(filter).AsQueryable();
            }
        }

        public IEnumerable<T> GetAll()
        {
            using(var context = new TContext())
            {
                return context.Set<T>().ToList();
            }
        }

        public T GetById(int id)
        {
            using(var context = new TContext())
            {
                return context.Set<T>().Find(id);
            }
        }

        public T GetOne(Expression<Func<T, bool>> filter)
        {
            using(var context = new TContext())
            {
                return context.Set<T>().Where(filter).SingleOrDefault();
            }
        }
        public void Create(T entity)
        {
            using (var context = new TContext())
            {
                context.Set<T>().Add(entity);
                context.SaveChanges();
            }
        }

        public void Delete(T entity)
        {
            using (var context = new TContext())
            {
                context.Set<T>().Remove(entity);
                context.SaveChanges();
            }
        }


        public void Update(T entity)
        {
            using(var context = new TContext())
            {
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public bool CreateByResult(T entity)
        {
            var result = 0;
            using (var context = new TContext())
            {
                context.Set<T>().Add(entity);
                result = context.SaveChanges();
            }
            return result == 1;
        }

        public bool UpdateByResult(T entity)
        {
            var result = 0;
            using (var context = new TContext())
            {
                context.Entry(entity).State = EntityState.Modified;
                result = context.SaveChanges();
            }
            return result == 1;
        }

        public bool DeleteByResult(T entity)
        {
            var result = 0;

            using (var context = new TContext())
            {
                context.Set<T>().Remove(entity);
                result = context.SaveChanges();
            }
            return result == 1;
        }
    }
}
