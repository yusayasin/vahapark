﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemorySalonDal : ISalonDal
    {
        private List<Salon> _salons = new List<Salon>
        {
            new Salon(){Id=1, Icon=@"fas fa-hotel", Title="Vaha Park Salon", Paragraph="Düğün davetleri ya da ürün lansmanı, sosyal davetler ya da konferanslar, toplantı ve ziyafet salonumuz her konuğumuzun hak ettiği konforu ve ilgiyi sonuna kadar deneyimleyeceği şekilde tasarlanmıştır.", IsEnable=true},
            new Salon(){Id=2, Icon=@"fas fa-chart-area", Title="Vaha Park Bahçe", Paragraph="Yeşilin, deniz manzarası ile bütünleştiği, eşsiz atmosferi ile bahçemiz kır düğünü konseptinde yaz düğünlerine ev sahipliği yapmaktadır.", IsEnable=true}
        };
        public IQueryable<Salon> GetAll(Expression<Func<Salon, bool>> filter)
        {
            return _salons.AsQueryable().Where(filter);
        }

        public IEnumerable<Salon> GetAll()
        {
            return _salons.ToList();
        }

        public Salon GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Salon GetOne(Expression<Func<Salon, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public void Create(Salon entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Salon entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Salon entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Salon> GetAllByFilter(Expression<Func<Salon, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(Salon entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(Salon entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(Salon entity)
        {
            throw new NotImplementedException();
        }

        IList<Salon> IRepository<Salon>.GetAllByFilter(Expression<Func<Salon, bool>> filter)
        {
            throw new NotImplementedException();
        }
    }
}
