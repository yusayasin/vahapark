﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemorySalonImageDal : ISalonImageDal
    {
        private List<SalonImage> _salonImgs = new List<SalonImage>
        {
            new SalonImage(){ Id = 1, SalonId = 1, Image="15temmuz.jpg", IsEnable=true },
            //new SalonImage(){ Id = 2, SalonId = 1, Image="1temmuz.jpg", IsEnable=true },
            new SalonImage(){ Id = 3, SalonId = 1, Image="1.svg", IsEnable=true },
            new SalonImage(){ Id = 4, SalonId = 2, Image="dugunMasasi.jpg", IsEnable=true },
            new SalonImage(){ Id = 5, SalonId = 2, Image="kirDugun.jpg", IsEnable=true },
        };

        public IQueryable<SalonImage> GetAll(Expression<Func<SalonImage, bool>> filter)
        {
            return _salonImgs.AsQueryable().Where(filter);
        }

        public IEnumerable<SalonImage> GetAll()
        {
            throw new NotImplementedException();
        }

        public IList<SalonImage> GetAllByFilter(Expression<Func<SalonImage, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public SalonImage GetById(int id)
        {
            throw new NotImplementedException();
        }

        public SalonImage GetOne(Expression<Func<SalonImage, bool>> filter)
        {
            throw new NotImplementedException();
        }
        
        public void Create(SalonImage entity)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(SalonImage entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(SalonImage entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(SalonImage entity)
        {
            throw new NotImplementedException();
        }

        public void Update(SalonImage entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(SalonImage entity)
        {
            throw new NotImplementedException();
        }
    }
}
