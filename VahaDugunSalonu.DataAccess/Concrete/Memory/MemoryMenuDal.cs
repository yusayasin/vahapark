﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemoryMenuDal : IMenuDal
    {
        private List<Menu> _menus = new List<Menu>
        {
            new Menu{ Id = 1, Image = "menu.jpg", Title = "Yemekli Menü", Paragraph = "Mercemek Çorbası, Et rosto, Pilav, Revani tatlısı", IsEnable = true },
            new Menu{ Id = 2, Image = "menu.jpg", Title = "Klasik Menü", Paragraph = "Yayla Çorbası, Büftek, Buhara Pilavı, Baklava", IsEnable = true },
            new Menu{ Id = 3, Image = "menu.jpg", Title = "Koktey Menü", Paragraph = "Pasta, Su böreği, Kuru pasta, Kadayıf", IsEnable = true },
            new Menu{ Id = 4, Image = "menu.jpg", Title = "Süper Menü", Paragraph = "Pasta, Kürt böreği, Gözleme, Güllaç", IsEnable = false }
        };

        public IQueryable<Menu> GetAll(Expression<Func<Menu, bool>> filter)
        {
            return _menus.AsQueryable().Where(filter);
        }

        public IEnumerable<Menu> GetAll()
        {
            return _menus;
        }

        public Menu GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Menu GetOne(Expression<Func<Menu, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public void Create(Menu entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Menu entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Menu entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Menu> GetAllByFilter(Expression<Func<Menu, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(Menu entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(Menu entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(Menu entity)
        {
            throw new NotImplementedException();
        }

        IList<Menu> IRepository<Menu>.GetAllByFilter(Expression<Func<Menu, bool>> filter)
        {
            throw new NotImplementedException();
        }
    }
}
