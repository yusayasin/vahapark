﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemoryServiceDal : IServiceDal
    {
        private List<Service> _services = new List<Service>
        {
            new Service(){Id=1, Icon=@"fa fa-user-circle-o mr-2", Title="Lorem", Paragraph="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", IsEnable=false},
            new Service(){Id=2, Icon="fas fa-utensils", Title="Yemekli Davetleriniz Toplantılarınız", Paragraph="Temiz Ve Leziz Menülerimiz İle 1000 Kişi Ve Üzeri Yemekli Davetlerinizde Hizmetinizdeyiz.", IsEnable=true},
            new Service(){Id=3, Icon="fas fa-moon", Title="Kına Gecesi Organizasyonu", Paragraph="Vaha Park'ta Kına Geceleriniz Eşşsiz Organizasyon Eşliğinde Unutlmaz.", IsEnable=true},
            new Service(){Id=4, Icon="fas fa-cut", Title="Sünnet Organizasyonlarınız", Paragraph="Sünnet Organizasyonlarınızda  Yepyeni Konseptler.", IsEnable=true},
            new Service(){Id=5, Icon="fas fa-ring", Title="Düğün Ve Nişan", Paragraph="Düğün Nişan Organizayonlarınızda Bizimle Fark Yaratın.", IsEnable=true}
        };
        public IQueryable<Service> GetAll(Expression<Func<Service, bool>> filter)
        {
            return _services.AsQueryable().Where(filter);
        }

        public IEnumerable<Service> GetAll()
        {
            throw new NotImplementedException();
        }

        public Service GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Service GetOne(Expression<Func<Service, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public void Create(Service entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Service entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Service entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Service> GetAllByFilter(Expression<Func<Service, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(Service entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(Service entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(Service entity)
        {
            throw new NotImplementedException();
        }

        IList<Service> IRepository<Service>.GetAllByFilter(Expression<Func<Service, bool>> filter)
        {
            throw new NotImplementedException();
        }
    }
}
