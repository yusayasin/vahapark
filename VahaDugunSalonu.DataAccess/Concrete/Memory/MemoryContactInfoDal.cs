﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemoryContactInfoDal : IContactInfoDal
    {
        private List<ContactInfo> _contactInfo = new List<ContactInfo>
        {
            new ContactInfo{
                Id=1, 
                Address="Cami Mah, Öğretmen Zatiye Çelemen Sok No:38, 41700 Darıca/Kocaeli", 
                Explanation="", 
                GoogleMap="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12088.811451804748!2d29.3781871!3d40.7575627!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf3023128ebdd4d!2zVmFoYSBQYXJrIETDvMSfw7xuICYgRGF2ZXQ!5e0!3m2!1str!2str!4v1615062209038!5m2!1str!2str", 
                Phone="+902627450101", 
                //Phone2="+905442037377",
                WorkTime="8:00-22:00",
                //Email = "vahaPark@gmail.com"
            }
        };

        public ContactInfo GetFirstContactInfo()
        {
            return _contactInfo.FirstOrDefault();
        }

        public IQueryable<ContactInfo> GetAll(Expression<Func<ContactInfo, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ContactInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public ContactInfo GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ContactInfo GetOne(Expression<Func<ContactInfo, bool>> filter)
        {
            throw new NotImplementedException();
        }
        public void Create(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        public void Update(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        public IList<ContactInfo> GetAllByFilter(Expression<Func<ContactInfo, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(ContactInfo entity)
        {
            throw new NotImplementedException();
        }
    }
}
