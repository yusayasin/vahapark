﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemoryAboutUsDal : IAboutUsDal
    {
        private List<AboutUs> _aboutUs = new List<AboutUs>
        {
            new AboutUs(){Id=1, Icon=@"fa fa-user-circle-o mr-2", Title="Lorem", Paragraph="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", IsEnable=false},
            new AboutUs(){Id=2, Icon=@"fas fa-image", Title="Manzara", Paragraph="Yeşilin, eşsiz deniz manzarası ile bütünleştiği Vaha Park Düğün & Davet convention center düğün, konferans ve balo salonu olarak hizmet vermektedir. İzmit körfez hattının başlangıcında yer alan Darıca ilçesinin sahil kıyısında konumlanmış, Türkiye'nin en prestijli düğün & davetlerinin ev sahipliğini yapmaktadır. Beş kişiden 1300 kişiye kadar tüm organizasyonlarınızı gerçekleştirebileceğiniz Vaha Park, düğünleriniz için “Düğün Planlama” bölümü uzmanları ile yapacağınız karşılıklı görüşmelerle kendi düğününüzü hayal ettiğiniz bir masala dönüştürebilirsiniz.", IsEnable=true},
            new AboutUs(){Id=3, Icon=@"fas fa-hotel", Title="Vaha Park Salon", Paragraph="Düğün davetleri ya da ürün lansmanı, sosyal davetler ya da konferanslar, toplantı ve ziyafet salonumuz her konuğumuzun hak ettiği konforu ve ilgiyi sonuna kadar deneyimleyeceği şekilde tasarlanmıştır.", IsEnable=true},
            new AboutUs(){Id=4, Icon=@"fas fa-chart-area", Title="Vaha Park Bahçe", Paragraph="Yeşilin, deniz manzarası ile bütünleştiği, eşsiz atmosferi ile bahçemiz kır düğünü konseptinde yaz düğünlerine ev sahipliği yapmaktadır.", IsEnable=true},
            new AboutUs(){Id=5, Icon=@"fas fa-map-marker-alt", Title="Konum", Paragraph="Vaha Park Düğün & Davet körfez hattının başlangıcında yer alan Darıca ilçesinin sahil kıyısında bulunmaktadır. Dudayev Park Sahil Otopark karşısında yer alan Vaha Park, Sabiha Gökçen Havalimanı’na sadece 25 dakika uzaklıktadır. Gebze'ye 10 dakika, Kadıköy'e ise 45 km mesafesindedir.", IsEnable=true}
        };
        public IQueryable<AboutUs> GetAll(Expression<Func<AboutUs, bool>> filter)
        {
            return _aboutUs.AsQueryable().Where(filter);
        }

        public IEnumerable<AboutUs> GetAll()
        {
            throw new NotImplementedException();
        }

        public AboutUs GetById(int id)
        {
            throw new NotImplementedException();
        }

        public AboutUs GetOne(Expression<Func<AboutUs, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public void Create(AboutUs entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(AboutUs entity)
        {
            throw new NotImplementedException();
        }

        public void Update(AboutUs entity)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(AboutUs entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(AboutUs entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(AboutUs entity)
        {
            throw new NotImplementedException();
        }

        IList<AboutUs> IRepository<AboutUs>.GetAllByFilter(Expression<Func<AboutUs, bool>> filter)
        {
            throw new NotImplementedException();
        }
    }
}
