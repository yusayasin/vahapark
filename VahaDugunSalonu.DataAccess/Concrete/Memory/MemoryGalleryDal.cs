﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.DataAccess.Concrete.Memory
{
    public class MemoryGalleryDal : IGalleryDal
    {
        private List<Gallery> _galleries = new List<Gallery>
        {
            new Gallery(){Id=1, Alt="15 Temmuz tebrik mesajı", ImageName="15temmuz.jpg", Title="", Paragraph="", IsEnable=false },
            //new Gallery(){Id=2, Alt="Aldığımız Önlemler", ImageName="1temmuz.jpg", Title="", Paragraph="", IsEnable=true },
            new Gallery(){Id=3, Alt="Babalar gününüz kutlu olsun", ImageName="babagun.jpg", Title="", Paragraph="", IsEnable=true },
            new Gallery(){Id=4, Alt="Mevlid Kandili", ImageName="mevlidKandili.jpg", Title="", Paragraph="", IsEnable=true },
            new Gallery(){Id=5, Alt="Salonlarımız", ImageName="dugunMasasi.jpg", Title="Salonlarımız", Paragraph="Nikah Merasimi; ister bahçe, ister salon, ister teras... hepsi birbirinden özel", IsEnable=true },
            new Gallery(){Id=6, Alt="Bahçe", ImageName="kirDugun.jpg", Title="", Paragraph="", IsEnable=true }
        }; 

        public IQueryable<Gallery> GetAll(Expression<Func<Gallery, bool>> filter)
        {
            return _galleries.AsQueryable().Where(filter);
        }

        public IEnumerable<Gallery> GetAll()
        {
            return _galleries;
        }

        public Gallery GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Gallery GetOne(Expression<Func<Gallery, bool>> filter)
        {
            throw new NotImplementedException();
        }
        public void Create(Gallery entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Gallery entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Gallery entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Gallery> GetAllByFilter(Expression<Func<Gallery, bool>> filter)
        {
            throw new NotImplementedException();
        }

        public bool CreateByResult(Gallery entity)
        {
            throw new NotImplementedException();
        }

        public bool UpdateByResult(Gallery entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteByResult(Gallery entity)
        {
            throw new NotImplementedException();
        }

        IList<Gallery> IRepository<Gallery>.GetAllByFilter(Expression<Func<Gallery, bool>> filter)
        {
            throw new NotImplementedException();
        }
    }
}
