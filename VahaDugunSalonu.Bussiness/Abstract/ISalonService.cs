﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface ISalonService
    {
        List<Salon> GetAll();
        List<Salon> GetAllByFilter(Expression<Func<Salon, bool>> expression);
        Salon GetById(int id);
        bool Create(Salon entity);
        bool Update(Salon entity);
        bool Delete(Salon entity);
    }
}
