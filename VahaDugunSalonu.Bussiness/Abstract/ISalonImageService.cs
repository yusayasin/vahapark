﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface ISalonImageService
    {
        List<SalonImage> GetAll();
        List<SalonImage> GetAllByFilter(Expression<Func<SalonImage, bool>> expression);
        SalonImage GetById(int id);
        bool Create(SalonImage entity);
        bool Update(SalonImage entity);
        bool Delete(SalonImage entity);
    }
}
