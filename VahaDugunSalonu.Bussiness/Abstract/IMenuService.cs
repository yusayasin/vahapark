﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface IMenuService
    {
        List<Menu> GetAll();
        List<Menu> GetAllByFilter(Expression<Func<Menu, bool>> expression);
        Menu GetById(int id);
        bool Create(Menu entity);
        bool Update(Menu entity);
        bool Delete(Menu entity);
    }
}
