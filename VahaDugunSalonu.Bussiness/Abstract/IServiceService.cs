﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface IServiceService
    {
        List<Service> GetAll();
        List<Service> GetAllByFilter(Expression<Func<Service, bool>> expression);
        Service GetById(int id);
        bool Create(Service entity);
        bool Update(Service entity);
        bool Delete(Service entity);
    }
}
