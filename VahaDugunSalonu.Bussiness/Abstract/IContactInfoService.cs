﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface IContactInfoService
    {
        ContactInfo GetFirstContactInfo();
        List<ContactInfo> GetAll();
        List<ContactInfo> GetAllByFilter(Expression<Func<ContactInfo, bool>> expression);
        ContactInfo GetById(int id);
        bool Create(ContactInfo entity);
        bool Update(ContactInfo entity);
    }
}
