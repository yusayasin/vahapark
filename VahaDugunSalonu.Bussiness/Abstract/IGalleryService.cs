﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface IGalleryService
    {
        List<Gallery> GetAll();
        List<Gallery> GetAllByFilter(Expression<Func<Gallery, bool>> expression);
        Gallery GetById(int id);
        bool Create(Gallery entity);
        bool Update(Gallery entity);
        bool Delete(Gallery entity);

    }
}
