﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Abstract
{
    public interface IAboutUsService
    {
        List<AboutUs> GetAll();
        List<AboutUs> GetAllByFilter(Expression<Func<AboutUs, bool>> expression);
        AboutUs GetById(int id);
        bool Create(AboutUs entity);
        bool Update(AboutUs entity);
        bool Delete(AboutUs entity);
    }
}
