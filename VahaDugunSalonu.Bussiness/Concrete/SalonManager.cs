﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class SalonManager : ISalonService
    {
        private ISalonDal _dal;

        public SalonManager(ISalonDal dal)
        {
            _dal = dal;
        }

        public List<Salon> GetAll()
        {
            return _dal.GetAll().ToList();
        }

        public List<Salon> GetAllByFilter(Expression<Func<Salon, bool>> expression)
        {
            //return _dal.GetAll(expression).ToList();//Database yoksa!
            return _dal.GetAllByFilter(expression).ToList();//Database varsa!
        }

        public Salon GetById(int id)
        {
            return _dal.GetById(id);
        }

        public bool Create(Salon entity)
        {
            return _dal.CreateByResult(entity);
        }


        public bool Delete(Salon entity)
        {
            return _dal.DeleteByResult(entity);
        }

        public bool Update(Salon entity)
        {
            return _dal.UpdateByResult(entity);
        }
    }
}
