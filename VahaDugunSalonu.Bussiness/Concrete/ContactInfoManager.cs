﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class ContactInfoManager : IContactInfoService
    {
        private IContactInfoDal _dal;
        public ContactInfoManager(IContactInfoDal dal)
        {
            _dal = dal;
        }
        public List<ContactInfo> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<ContactInfo> GetAllByFilter(Expression<Func<ContactInfo, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public ContactInfo GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ContactInfo GetFirstContactInfo()
        {
            return _dal.GetFirstContactInfo();
        }
        public bool Create(ContactInfo entity)
        {
            return _dal.CreateByResult(entity);
        }

        public void Update(ContactInfo entity)
        {
            throw new NotImplementedException();
        }

        bool IContactInfoService.Update(ContactInfo entity)
        {
            return _dal.UpdateByResult(entity);
        }
    }
}
