﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class SalonImageManager : ISalonImageService
    {
        private ISalonImageDal _dal;

        public SalonImageManager(ISalonImageDal dal)
        {
            _dal = dal;
        }

        public List<SalonImage> GetAll()
        {
            return _dal.GetAll().ToList();
        }

        public List<SalonImage> GetAllByFilter(Expression<Func<SalonImage, bool>> expression)
        {
            //return _dal.GetAll(expression).ToList();//Database yoksa!
            return _dal.GetAllByFilter(expression).ToList();//Database varsa!
        }

        public SalonImage GetById(int id)
        {
            return _dal.GetById(id);
        }

        public bool Create(SalonImage entity)
        {
            return _dal.CreateByResult(entity);
        }

        public bool Delete(SalonImage entity)
        {
            return _dal.DeleteByResult(entity);
        }

        public bool Update(SalonImage entity)
        {
            return _dal.UpdateByResult(entity);
        }
    }
}
