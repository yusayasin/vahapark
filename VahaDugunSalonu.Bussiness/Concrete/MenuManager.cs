﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class MenuManager : IMenuService
    {
        private IMenuDal _menuDal;
        public MenuManager(IMenuDal menuDal)
        {
            _menuDal = menuDal;
        }

        public List<Menu> GetAll()
        {
            return _menuDal.GetAll().ToList();
        }

        public List<Menu> GetAllByFilter(Expression<Func<Menu, bool>> expression)
        {
            //return _menuDal.GetAll(expression).ToList();//Database yoksa!
            return _menuDal.GetAllByFilter(expression).ToList();//database varsa!
        }

        public Menu GetById(int id)
        {
            return _menuDal.GetById(id);
        }
        public bool Create(Menu entity)
        {
            return _menuDal.CreateByResult(entity);
        }

        public bool Update(Menu entity)
        {
            return _menuDal.UpdateByResult(entity);
        }

        public bool Delete(Menu entity)
        {
            return _menuDal.DeleteByResult(entity);
        }
    }
}
