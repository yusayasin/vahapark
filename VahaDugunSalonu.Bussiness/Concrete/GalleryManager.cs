﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class GalleryManager : IGalleryService
    {
        private IGalleryDal _dal;
        public GalleryManager(IGalleryDal dal)
        {
            _dal = dal;
        }

        public List<Gallery> GetAll() 
        {
            return _dal.GetAll().ToList();
        }

        public List<Gallery> GetAllByFilter(Expression<Func<Gallery, bool>> expression)
        {
            //return _dal.GetAll(expression).ToList();//Database yoksa!
            return _dal.GetAllByFilter(expression).ToList();//Database varsa!
        }

        public Gallery GetById(int id)
        {
            return _dal.GetById(id);
        }

        public bool Create(Gallery entity)
        {
            return _dal.CreateByResult(entity);
        }

        public bool Update(Gallery entity)
        {
            return _dal.UpdateByResult(entity);
        }

        public bool Delete(Gallery entity)
        {
            return _dal.DeleteByResult(entity);
        }
    }
}
