﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class ServiceManager : IServiceService
    {
        private IServiceDal _dal;
        public ServiceManager(IServiceDal dal)
        {
            _dal = dal;
        }

        public List<Service> GetAll()
        {
            return _dal.GetAll().ToList();
        }

        public List<Service> GetAllByFilter(Expression<Func<Service, bool>> expression)
        {
            //return _dal.GetAll(expression).ToList();//Database yoksa!
            return _dal.GetAllByFilter(expression).ToList();//Database varsa
        }

        public Service GetById(int id)
        {
            return _dal.GetById(id);
        }

        public bool Create(Service entity)
        {
            return _dal.CreateByResult(entity);
        }

        public void Update(Service entity)
        {
            _dal.Update(entity);
        }

        bool IServiceService.Update(Service entity)
        {
            return _dal.UpdateByResult(entity);
        }

        public bool Delete(Service entity)
        {
            return _dal.DeleteByResult(entity);
        }
    }
}
