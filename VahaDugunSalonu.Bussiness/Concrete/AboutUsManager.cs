﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Bussiness.Concrete
{
    public class AboutUsManager : IAboutUsService
    {
        private IAboutUsDal _dal;

        public AboutUsManager(IAboutUsDal dal)
        {
            _dal = dal;
        }


        public List<AboutUs> GetAllByFilter(Expression<Func<AboutUs, bool>> expression)
        {
            //return _dal.GetAll(expression).ToList();//Database yoksa!
            return _dal.GetAllByFilter(expression).ToList();//Database varsa!
        }

        public List<AboutUs> GetAll()
        {
            return _dal.GetAll().ToList();
        }

        public AboutUs GetById(int id)
        {
            return _dal.GetById(id);
        }

        public void Create(AboutUs entity)
        {
            _dal.Create(entity);
        }

        public void Update(AboutUs entity)
        {
            _dal.Update(entity);
        }

        public void Delete(AboutUs entity)
        {
            _dal.Delete(entity);
        }

        bool IAboutUsService.Create(AboutUs entity)
        {
            return _dal.CreateByResult(entity);
        }

        bool IAboutUsService.Update(AboutUs entity)
        {
            return _dal.UpdateByResult(entity);
        }

        bool IAboutUsService.Delete(AboutUs entity)
        {
            return _dal.DeleteByResult(entity);
        }
    }
}
