#pragma checksum "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "432e2f15375915e79b39c0f29ff3861ce370923d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Contact), @"mvc.1.0.view", @"/Views/Home/Contact.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\_ViewImports.cshtml"
using VahaDugunSalonu;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\_ViewImports.cshtml"
using VahaDugunSalonu.Help;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\_ViewImports.cshtml"
using VahaDugunSalonu.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"432e2f15375915e79b39c0f29ff3861ce370923d", @"/Views/Home/Contact.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"666b799923363ff4087e35844fdba29cce7e3355", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Contact : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ContactInfoModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
  
    ViewData["Title"] = "İletişim";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<!-- Section heading -->\r\n<h1>Bize Ulaşın</h1>\r\n<!-- Section: Contact v.1 -->\r\n<section class=\"py-5 header\">\r\n\r\n    <!-- Section description -->\r\n");
#nullable restore
#line 14 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
     if (!string.IsNullOrWhiteSpace(Model.ContactInfo.Explanation))
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <p class=\"text-center w-responsive mx-auto pb-5\">\r\n            ");
#nullable restore
#line 17 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
       Write(Model.ContactInfo.Explanation);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </p>\r\n");
#nullable restore
#line 19 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
    <!-- Grid row -->
    <div class=""row"">

        <!-- Grid column -->
        <div class=""col-lg-5 mb-lg-0 mb-4"">

            <!-- Form with header -->
            <!-- Card -->
            <div class=""card"">

                <!-- Card content -->
                <div class=""card-body"">

                    <div class=""col"">
                        <a class=""btn-floating blue accent-1"">
                            <i class=""fas1 fas fa-map-marker-alt""></i>
                        </a>
                        <p>");
#nullable restore
#line 38 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
                      Write(Model.ContactInfo.Address);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n");
            WriteLiteral(@"                    </div>
                    </br>
                    <div class=""col"">
                        <a class=""btn-floating blue accent-1"">
                            <i class=""fas1 fas fa-phone""></i>
                        </a>
                        <p>");
#nullable restore
#line 46 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
                      Write(Model.ContactInfo.Phone);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                        <p class=\"mb-md-0\">");
#nullable restore
#line 47 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
                                      Write(Model.ContactInfo.Phone2);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                    </div>
                    </br>
                    <div class=""col"">
                        <a class=""btn-floating blue accent-1"">
                            <i class=""fas1 fas fa-envelope""></i>
                        </a>
                        <p class=""mb-0"">");
#nullable restore
#line 54 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
                                   Write(Model.ContactInfo.Email);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                    </div>
                    </br>
                </div>

            </div>
            <!-- Card -->
            <!-- Form with header -->

        </div>
        <!-- Grid column -->
        <!-- Grid column -->
        <div class=""col-lg-7"">

            <!--Google map-->
            <div id=""map-container-section"" class=""z-depth-1-half map-container-section"">
                <iframe");
            BeginWriteAttribute("src", " src=\"", 2235, "\"", 2269, 1);
#nullable restore
#line 70 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Home\Contact.cshtml"
WriteAttributeValue("", 2241, Model.ContactInfo.GoogleMap, 2241, 28, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" frameborder=\"0\"\r\n                        style=\"border:0\" style=\"border:0;\"");
            BeginWriteAttribute("allowfullscreen", " allowfullscreen=\"", 2346, "\"", 2364, 0);
            EndWriteAttribute();
            WriteLiteral(" loading=\"lazy\"></iframe>\r\n            </div>\r\n\r\n");
            WriteLiteral("\r\n        </div>\r\n        <!-- Grid column -->\r\n\r\n    </div>\r\n    <!-- Grid row -->\r\n\r\n</section>\r\n<!-- Section: Contact v.1 -->\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ContactInfoModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
