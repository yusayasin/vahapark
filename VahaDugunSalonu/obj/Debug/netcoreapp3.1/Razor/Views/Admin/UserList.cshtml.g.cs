#pragma checksum "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2541222ede3e30395a68e4500254022d6fc7d293"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_UserList), @"mvc.1.0.view", @"/Views/Admin/UserList.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\_ViewImports.cshtml"
using VahaDugunSalonu;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\_ViewImports.cshtml"
using VahaDugunSalonu.Help;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\_ViewImports.cshtml"
using VahaDugunSalonu.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2541222ede3e30395a68e4500254022d6fc7d293", @"/Views/Admin/UserList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"666b799923363ff4087e35844fdba29cce7e3355", @"/Views/_ViewImports.cshtml")]
    #nullable restore
    public class Views_Admin_UserList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<UserListModel>
    #nullable disable
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
  
    ViewData["Title"] = "UserList";
    Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>UserList</h1>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-4\"><b>Ad</b></div>\r\n    <div class=\"col-4\"><b>Email</b></div>\r\n    <div class=\"col-4\"></div>\r\n</div>\r\n");
#nullable restore
#line 14 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
  
    int count = 0;
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
     foreach (var item in Model.Users)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div");
            BeginWriteAttribute("style", " style=\"", 365, "\"", 423, 2);
            WriteAttributeValue("", 373, "background-color:", 373, 17, true);
#nullable restore
#line 18 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
WriteAttributeValue("", 390, count%2==0?"burlywood":"white", 390, 33, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" class=\"row\">\r\n            <div class=\"col-4\">");
#nullable restore
#line 19 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
                          Write(item.FullName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n            <div class=\"col-4\">");
#nullable restore
#line 20 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
                          Write(item.Email);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n            <div class=\"col-4\">\r\n");
#nullable restore
#line 22 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
                 if (!Model.UserManager.IsInRoleAsync(item, "admin").Result)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                 <a");
            BeginWriteAttribute("href", " href=\"", 691, "\"", 727, 2);
            WriteAttributeValue("", 698, "/admin/DeleteUser?id=", 698, 21, true);
#nullable restore
#line 24 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
WriteAttributeValue("", 719, item.Id, 719, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Delete</a>\r\n");
#nullable restore
#line 25 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </div>\r\n        </div>\r\n");
#nullable restore
#line 28 "C:\Users\yusay\source\repos\VahaDugunSalonu\VahaDugunSalonu\Views\Admin\UserList.cshtml"
        count++;
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
        }
        #pragma warning restore 1998
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; } = default!;
        #nullable disable
        #nullable restore
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<UserListModel> Html { get; private set; } = default!;
        #nullable disable
    }
}
#pragma warning restore 1591
