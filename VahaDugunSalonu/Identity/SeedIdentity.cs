﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace VahaDugunSalonu.Identity
{
    public static class SeedIdentity
    {
        public static async Task Seed(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            var username = configuration["Data:AdminUser:username"];
            var Email = configuration["Data:AdminUser:email"];
            var password = configuration["Data:AdminUser:password"];
            var role = configuration["Data:AdminUser:role"];


            //Eğer sysadmin user yoksa Yeni sysadmin user oluştur
            if (await userManager.FindByNameAsync(username) == null)
            {
                //role manager oluşturur => AspNetRoles
                await roleManager.CreateAsync(new IdentityRole(role));

                var user = new ApplicationUser()
                {
                    UserName = username,
                    Email = Email,
                    FullName = "sysadmin",
                    EmailConfirmed = true
                };
                //user oluşturur => AspNetUsers
                var result = await userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    //user'a role ekliyor => AspNetUserRoles
                    await userManager.AddToRoleAsync(user, role);
                }
            }
        }
    }
}
