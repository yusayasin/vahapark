﻿using Microsoft.AspNetCore.Identity;

namespace VahaDugunSalonu.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
    }
}
