﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Edi.Captcha;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.Entities;
using VahaDugunSalonu.Help;
using VahaDugunSalonu.Models;

namespace VahaDugunSalonu.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IGalleryService _galleryService;
        private IContactInfoService _contactInfoService;
        private IAboutUsService _aboutUsService;
        private ISalonService _salonService;
        private ISalonImageService _salonImageService;
        private IServiceService _serviceService;
        private IMenuService _menuService;
        private readonly IConfiguration _config;
        //Captcha
        private readonly ISessionBasedCaptcha _captcha;

        public HomeController(ILogger<HomeController> logger, 
            IGalleryService galleryService, IContactInfoService contactInfoService,
            IAboutUsService aboutUsService, 
            ISalonService salonService, ISalonImageService salonImageService,
            IServiceService serviceService, IMenuService menuService,
            IConfiguration config, ISessionBasedCaptcha captcha)
        {
            _logger = logger;
            _config = config;
            //Captcha
            _captcha = captcha;
            _galleryService = galleryService;
            _contactInfoService = contactInfoService;
            _aboutUsService = aboutUsService;
            _salonService = salonService;
            _salonImageService = salonImageService;
             _serviceService = serviceService;
            _menuService = menuService;
        }

        public IActionResult Index()
        {
            GalleryModel galleryModel = new GalleryModel();
            Expression<Func<Gallery, bool>> expression = s => s.IsEnable == true;
            galleryModel.Galleries = _galleryService.GetAllByFilter(expression);

            return View(galleryModel);
        }

        public IActionResult AboutUs()
        {
            AboutUsModel aboutUsModel = new AboutUsModel();
            Expression<Func<AboutUs, bool>> expression = s => s.IsEnable == true;
            aboutUsModel.AboutUss = _aboutUsService.GetAllByFilter(expression);
            return View(aboutUsModel);
        }

        public IActionResult Salon()
        {
            SalonModel salonModel = new SalonModel();

            Expression<Func<Salon, bool>> expression = s => s.IsEnable == true;
            salonModel.Salons = _salonService.GetAllByFilter(expression);

            Expression<Func<SalonImage, bool>> expression1 = s => s.IsEnable == true;
            salonModel.SalonImages = _salonImageService.GetAllByFilter(expression1);

            return View(salonModel);
        }

        public IActionResult Servis()
        {
            ServiceModel serviceModel = new ServiceModel();
            Expression<Func<Service, bool>> expression = s => s.IsEnable == true;
            serviceModel.Services = _serviceService.GetAllByFilter(expression);
            return View(serviceModel);
        }

        public IActionResult Menu()
        {
            MenuModel menuModel = new MenuModel();
            Expression<Func<Menu, bool>> expression = s => s.IsEnable == true;
            menuModel.Menus = _menuService.GetAllByFilter(expression);
            return View(menuModel);
        }

        public IActionResult Contact()
        {
            ContactInfoModel contactInfoModel = new ContactInfoModel();
            contactInfoModel.ContactInfo = _contactInfoService.GetFirstContactInfo();
            if (contactInfoModel.ContactInfo == null)
                contactInfoModel.ContactInfo = new ContactInfo();
            return View(contactInfoModel);
        }

        public IActionResult Reservation()
        {
            ReservationModel model = new ReservationModel();
            model.Date = DateTime.Now;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Reservation(ReservationModel model)
        {
            //Validate Google recaptcha here
            //var response = Request["g-recaptcha-response"];
            //string secretKey = "6LdowbEaAAAAAPU2-xCGJyuEFeZJFqFyyMGwjVsb";

            if (ModelState.IsValid)
            {
                //Captcha
                bool isValidCaptcha = _captcha.Validate(model.CaptchaCode, HttpContext.Session);
                if (isValidCaptcha)
                {
                    //SendReservationEmail
                    await new SendEmail(_config).SendReservationEmail(model);
                    TempData.Put("message", new ResultMessage
                    {
                        Title = "Rezervasyon mesajı",
                        Message = "Mesajınız başarıyla gönderildi. En kısa zamanda size dönülecektir.",
                        Css = "success"
                    });
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData.Put("message", new ResultMessage
                    {
                        Title = "Güvenlik Kodu Girilmedi",
                        Message = "Lutfen güvenlik kodunu giriniz!",
                        Css = "error"
                    });
                    return View(model);
                }
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hatalı Rezervasyon Formu",
                    Message = "Lutfen istenilen alanları doldurunuz!",
                    Css = "danger"
                });
            }
            return View(model);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
