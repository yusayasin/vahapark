﻿using Edi.Captcha;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.Entities;
using VahaDugunSalonu.Help;
using VahaDugunSalonu.Identity;
using VahaDugunSalonu.Models;

namespace VahaDugunSalonu.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class AdminController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IGalleryService _galleryService;
        private IContactInfoService _contactInfoService;
        private IAboutUsService _aboutUsService;
        private ISalonService _salonService;
        private ISalonImageService _salonImageService;
        private IServiceService _serviceService;
        private IMenuService _menuService;

        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;

        private readonly IConfiguration _config;
        //Captcha
        private readonly ISessionBasedCaptcha _captcha;
        public AdminController(ILogger<HomeController> logger,
            IGalleryService galleryService, IContactInfoService contactInfoService,
            IAboutUsService aboutUsService, 
            ISalonService salonService, ISalonImageService salonImageService,
            IServiceService serviceService, IMenuService menuService,
            UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            IConfiguration config, ISessionBasedCaptcha captcha)
        {
            _logger = logger;
            _galleryService = galleryService;
            _contactInfoService = contactInfoService;
            _aboutUsService = aboutUsService;
            _salonService = salonService;
            _salonImageService = salonImageService;
            _serviceService = serviceService;
            _menuService = menuService;
            _userManager = userManager;
            _signInManager = signInManager;
            _config = config;
            //Captcha
            _captcha = captcha;
        }


        /// <summary>
        /// Generate Token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private string GenerateToken(ApplicationUser user)
        {
            //generate token
            var tokenCode = _userManager.GenerateEmailConfirmationTokenAsync(user);
            var callbackUrl = Url.Action("ConfirmEmail", "admin", new
            {
                userId = user.Id,
                token = tokenCode.Result
            });
            return callbackUrl;
        }



        [Authorize(Roles ="admin")]
        public IActionResult Register()
        {
            return View(new RegisterModel());
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel registerModel)
        {
            if (!ModelState.IsValid)
            {
                return View(registerModel);
            }

            var user = new ApplicationUser
            {
                UserName = registerModel.UserName,
                Email = registerModel.Email,
                FullName = registerModel.FullName
            };

            var result = await _userManager.CreateAsync(user, registerModel.Password);

            if (result.Succeeded)
            {
                //generate token
                string callbackUrl = GenerateToken(user);
                //send email
                await new SendEmail(_config).SendConfirmEmail(user, callbackUrl);

                TempData.Put("message", new ResultMessage()
                {
                    Title = "Hesap Onayı",
                    Message = "Eposta adrenize gelen link ile hesabınızı onaylayınız",
                    Css = "warning"
                });

                return RedirectToAction("UserList");
            }
            return View(registerModel);
        }
                
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                TempData.Put("message", new ResultMessage()
                {
                    Title = "Hesap Onayı",
                    Message = "Hesap onayı için bilgileriniz yanlış",
                    Css = "danger"
                });
                return Redirect("~/");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if(user != null)
            {
                //Onay'a düşüyor ve onaylandığında confirm olduğunu database'e kaydediyor
                var result = await _userManager.ConfirmEmailAsync(user, token);
                if (result.Succeeded)
                {
                    TempData.Put("message", new ResultMessage
                    {
                        Title = "Hesap Onayı",
                        Message = "Hesabınız başarıyla onaylanmıştır",
                        Css = "success"
                    });
                    return RedirectToAction("LogIn");
                }
            }
            TempData.Put("message", new ResultMessage
            {
                Title = "Hesap Onayı",
                Message = "Hesabınız onaylanamadı.",
                Css = "danger"
            });
            return Redirect("~/");
        }

        public async Task<IActionResult> ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ForgotPassword(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Forgot Password",
                    Message = "Email adresini boş geçemezsiniz!",
                    Css = "danger"
                });
                ViewBag.ErrorMessage = "Email adresini boş geçemezsiniz!";
                return View();
            }
            var user = await _userManager.FindByEmailAsync(email);
            if(user == null)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Forgot Password",
                    Message = "Bu Email adresi ile kullanıcı bulunamadı!",
                    Css = "danger"
                });
                ViewBag.ErrorMessage = "Kayıtlı Email adresi giriniz!";
            }
            var tokenCode = _userManager.GeneratePasswordResetTokenAsync(user);
            var callBackUrl = Url.Action("ResetPassword", "Admin", new
            {
                token = tokenCode.Result
            });
            //Send email
            await new SendEmail(_config).SendEmailToResetPassword(user, callBackUrl);

            TempData.Put("message", new ResultMessage
            {
                Title = "Forgot Password",
                Message = "Parola yenilemek için email hesabınıza mail gönderildi!",
                Css = "warning"
            });
            return RedirectToAction("LogIn", "admin");
        }

        public IActionResult ResetPassword(string token)
        {
            if(string.IsNullOrEmpty(token))
            {
                return RedirectToAction("Gallery", "Home");
            }
            var model = new ResetPasswordModel { Token = token };
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                if(model.Password != model.RePassword)
                {
                    TempData.Put("message", new ResultMessage
                    {
                        Title = "Reset Password",
                        Message = "Şifreler aynı olmalı",
                        Css = "danger"
                    });
                }
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Reset Password",
                    Message = "Bu email ile kullanıcı bulunamadı!",
                    Css = "danger"
                });
                return RedirectToAction("Gallery", "Home");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (result.Succeeded)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Reset Password",
                    Message = "Şifre yenilendi!",
                    Css = "success"
                });
                return RedirectToAction("LogIn", "admin");
            }
            TempData.Put("message", new ResultMessage
            {
                Title = "Reset Password",
                Message = "Şifre yenilenemedi!",
                Css = "danger"
            });
            return View(model);
        }

        [Authorize(Roles ="admin")]
        public async Task<IActionResult> UserList() 
        {
            UserListModel model = new UserListModel();
            model.Users = _userManager.Users.ToList();
            model.UserManager = _userManager;
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public async Task<IActionResult> DeleteUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            var result = await _userManager.DeleteAsync(user);
            if (result.Succeeded)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Kullanıcı Silme İşlemi",
                    Message = user.FullName + " Kullanıcısı Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Kullanıcı Silme İşlemi",
                    Message = user.FullName + " Kullanıcısı Silinmedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("UserList");
        }

        public IActionResult AccessDenied(string ReturnUrl)
        {
            TempData.Put("message", new ResultMessage
            {
                Title = "Giriş Rededildi",
                Message = $"Linki {ReturnUrl} olan bu sayfaya giriş yetkiniz yok",
                Css = "danger"
            });
            return View();
        }

        #region LogIn & LogOut


        public async Task<IActionResult> LogIn(string returnUrl = null)
        {
            LoginModel loginModel = new LoginModel();
            loginModel.ReturnUrl = returnUrl ?? "/Admin/Gallery";

            return View(loginModel);
        }
        [HttpPost]
        public async Task<IActionResult> LogIn(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                //Captcha
                bool isValidCaptcha = _captcha.Validate(model.CaptchaCode, HttpContext.Session);
                if (isValidCaptcha)
                {
                    //username ile user'ı çağır
                    var user = await _userManager.FindByNameAsync(model.UserName);
                    //username ile bulamadı ise email ile kontrol et.
                    if (user == null)
                    {
                        user = await _userManager.FindByEmailAsync(model.UserName);
                    }

                    if (user == null)
                    {
                        ModelState.AddModelError("", "Bu kullanıcı ile daha önce hesap oluşturulmamış!");
                        return View(model);
                    }
                    //Email ile onaylanmış mı bak
                    if (!await _userManager.IsEmailConfirmedAsync(user))
                    {
                        ModelState.AddModelError("", "Lutfen hesabınızı email ile onaylayınız.");
                    }
                    //Şifre ile login ol
                    var result = await _signInManager.PasswordSignInAsync(user, model.Password, model.IsPersistent, false);
                    //Başarılı ise giriş yap
                    if (result.Succeeded)
                    {

                        TempData.Put("message", new ResultMessage()
                        {
                            Title = "Hoşgeldiniz.",
                            Message = "Hesabınıza Giriş Yapıldı.",
                            Css = "success"
                        });
                        return Redirect(model.ReturnUrl);
                    }
                }
                else
                {
                    TempData.Put("message", new ResultMessage
                    {
                        Title = "Güvenlik Kodu Girilmedi",
                        Message = "Lutfen güvenlik kodunu giriniz!",
                        Css = "error"
                    });
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
            return View(model);
        }

        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();

            TempData.Put("message", new ResultMessage()
            {
                Title = "Oturum Kapatıldı.",
                Message = "Hesabınız güvenli bir şekilde sonlandırıldı.",
                Css = "warning"
            });
            return Redirect("~/");
        }


        #endregion

        #region Gallery


        [Authorize]
        public IActionResult Gallery()
        {
            GalleryModel model = new GalleryModel();
            model.Galleries = _galleryService.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult GalleryDetails(int id)
        {
            GalleryModel model = new GalleryModel();
            model.SelectedGallery = _galleryService.GetById(id);
            return View(model);
        }

        [Authorize]
        public IActionResult GalleryEdit(int id)
        {
            GalleryModel model = new GalleryModel();
            model.SelectedGallery = _galleryService.GetById(id);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> GalleryEdit(GalleryModel model, IFormFile file)
        {
            //Resim Eklenme işlemi
            if (file != null)
            {
                model.SelectedGallery.ImageName = file.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            bool result = false;
            if (ModelState.IsValid)
                result = _galleryService.Update(model.SelectedGallery);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedGallery.Title + "\" başlıklı Resim Güncellenmesi",
                    Message = "Hizmet Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedGallery.Title + "\" başlıklı Resim Güncellenmesi",
                    Message = "Hizmet Güncellenemedi",
                    Css = "danger"
                });
            }
            return RedirectToAction("Gallery", "Admin");
        }

        [Authorize]
        public IActionResult GalleryDelete(int id)
        {
            Gallery entity = _galleryService.GetById(id);
            var result = _galleryService.Delete(entity);

            if (result)
            {
                //Resimi gidip yerinden siliyoruz
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", entity.ImageName);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Resim Silme İşlemi",
                    Message = entity.Title + " Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Resim Silme İşlemi",
                    Message = entity.Title + " Silinmedi!",
                    Css = "danger"
                });
            }
            return RedirectToAction("Gallery", "Admin");
        }

        [Authorize]
        public IActionResult GalleryCreate()
        {
            GalleryModel model = new GalleryModel();
            model.SelectedGallery = new Gallery();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> GalleryCreate(GalleryModel model, IFormFile file)
        {
            //Resim Eklenme işlemi
            if (file != null)
            {
                model.SelectedGallery.ImageName = file.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            bool result = false;
            if (ModelState.IsValid)
                result = _galleryService.Create(model.SelectedGallery);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedGallery.Title + "\" başlıklı kayıt eklendi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedGallery.Title + "\" başlıklı kayıt eklenemedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("Gallery", "Admin");
        }


        #endregion

        #region AboutUs


        [Authorize]
        public IActionResult AboutUs()
        {
            AboutUsModel model = new AboutUsModel();
            model.AboutUss = _aboutUsService.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult AboutUsDetails(int id)
        {
            AboutUsModel model = new AboutUsModel();
            model.SelectedAboutUs = _aboutUsService.GetById(id);
            return View(model);
        }

        [Authorize]
        public IActionResult AboutUsEdit(int id)
        {
            AboutUsModel model = new AboutUsModel();
            model.SelectedAboutUs = _aboutUsService.GetById(id);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult AboutUsEdit(AboutUsModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
                result = _aboutUsService.Update(model.SelectedAboutUs);
            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hakkımızda Güncellenmesi",
                    Message = "Hakkımızda Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hakkımızda Güncellenmesi",
                    Message = "Hakkımızda Güncellenemedi",
                    Css = "danger"
                });
            }
            return RedirectToAction("AboutUs", "Admin");
        }
        
        [Authorize]
        public IActionResult AboutUsDelete(int id)
        {
            AboutUs entity = _aboutUsService.GetById(id);
            var result = _aboutUsService.Delete(entity);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hakkımızda Silme İşlemi",
                    Message = entity.Title + " Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hakkımızda Silme İşlemi",
                    Message = entity.Title + " Silinmedi!",
                    Css = "danger"
                });
            }
            return RedirectToAction("AboutUs", "Admin");
        }

        [Authorize]
        public IActionResult AboutUsCreate()
        {
            AboutUsModel model = new AboutUsModel();
            model.SelectedAboutUs = new AboutUs();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult AboutUsCreate(AboutUsModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
                result = _aboutUsService.Create(model.SelectedAboutUs);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedAboutUs.Title + "\" başlıklı kayıt eklendi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedAboutUs.Title + "\" başlıklı kayıt eklenemedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("AboutUs", "Admin");
        }


        #endregion

        #region Salon


        [Authorize]
        public IActionResult Salon()
        {
            SalonModel model = new SalonModel();
            model.Salons = _salonService.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult SalonDetails(int id)
        {
            SalonModel model = new SalonModel();
            model.SelectedSalon = _salonService.GetById(id);
            return View(model);
        }

        [Authorize]
        public IActionResult SalonEdit(int id)
        {
            SalonModel model = new SalonModel();
            model.SelectedSalon = _salonService.GetById(id);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult SalonEdit(SalonModel model)
        {
            bool result = false;
            if (ModelState.IsValid) 
                result = _salonService.Update(model.SelectedSalon);
            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\""+ model.SelectedSalon.Title +"\" başlıklı Salon Güncellenmesi",
                    Message = "Salon Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedSalon.Title + "\" başlıklı Salon Güncellenmesi",
                    Message = "Salon Güncellenemedi",
                    Css = "danger"
                });
            }
            return RedirectToAction("Salon", "Admin");
        }

        [Authorize]
        public IActionResult SalonDelete(int id)
        {
            Salon entity = _salonService.GetById(id);
            string title = entity.Title;
            var result = _salonService.Delete(entity);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Salon Silme İşlemi",
                    Message = title + " Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Salon Silme İşlemi",
                    Message = title + " Silinmedi!",
                    Css = "danger"
                });
            }
            return RedirectToAction("Salon", "Admin");
        }

        [Authorize]
        public IActionResult SalonCreate()
        {
            SalonModel model = new SalonModel();
            model.SelectedSalon = new Salon();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult SalonCreate(SalonModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
                result = _salonService.Create(model.SelectedSalon);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedSalon.Title + "\" başlıklı kayıt eklendi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedSalon.Title + "\" başlıklı kayıt eklenemedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("Salon", "Admin");
        }


        #endregion

        #region SalonImage


        [Authorize]
        public IActionResult SalonImage()
        {
            SalonModel model = new SalonModel();
            Expression<Func<Salon, bool>> expression = s => s.IsEnable == true;
            model.Salons = _salonService.GetAllByFilter(expression);
            model.SalonImages = _salonImageService.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult SalonImageEdit(int id)
        {
            SalonModel model = new SalonModel();
            Expression<Func<Salon, bool>> expression = s => s.IsEnable == true;
            model.Salons = _salonService.GetAllByFilter(expression);
            model.SelectedSalonImage = _salonImageService.GetById(id);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SalonImageEdit(SalonModel model, IFormFile file)
        {
            //Resim Eklenme işlemi
            if (file != null)
            {
                model.SelectedSalonImage.Image = file.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            bool result = false;
            if (ModelState.IsValid)
                result = _salonImageService.Update(model.SelectedSalonImage);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Resim Güncellenmesi",
                    Message = "Hizmet Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Resim Güncellenmesi",
                    Message = "Hizmet Güncellenemedi",
                    Css = "danger"
                });
            }
            return RedirectToAction("SalonImage", "Admin");
        }

        [Authorize]
        public IActionResult SalonImageDelete(int id)
        {
            SalonImage entity = _salonImageService.GetById(id);
            var result = _salonImageService.Delete(entity);

            if (result)
            {
                //Resimi gidip yerinden siliyoruz
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", entity.Image);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }
            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Resim Silme İşlemi",
                    Message = entity.Image + " Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Resim Silme İşlemi",
                    Message = entity.Image + " Silinmedi!",
                    Css = "danger"
                });
            }
            return RedirectToAction("SalonImage", "Admin");
        }

        [Authorize]
        public IActionResult SalonImageCreate()
        {
            SalonModel model = new SalonModel();
            Expression<Func<Salon, bool>> expression = s => s.IsEnable == true;
            model.Salons = _salonService.GetAllByFilter(expression);
            model.SelectedSalonImage = new SalonImage();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SalonImageCreate(SalonModel model, IFormFile file)
        {
            //Resim Eklenme işlemi
            if (file != null)
            {
                model.SelectedSalonImage.Image = file.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            bool result = false;
            if (ModelState.IsValid)
                result = _salonImageService.Create(model.SelectedSalonImage);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedSalonImage.Image + "\" resim kayıtı eklendi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedSalonImage.Image + "\" resim kayıtı eklenemedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("SalonImage", "Admin");
        }


        #endregion

        #region Services


        [Authorize]
        public IActionResult Service()
        {
            ServiceModel model = new ServiceModel();
            model.Services = _serviceService.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult ServiceDetails(int id)
        {
            ServiceModel model = new ServiceModel();
            model.SelectedService = _serviceService.GetById(id);
            return View(model);
        }

        [Authorize]
        public IActionResult ServiceEdit(int id)
        {
            ServiceModel model = new ServiceModel();
            model.SelectedService = _serviceService.GetById(id);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult ServiceEdit(ServiceModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
                result = _serviceService.Update(model.SelectedService);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedService.Title + "\" başlıklı Hizmet Güncellenmesi",
                    Message = "Hizmet Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedService.Title + "\" başlıklı Hizmet Güncellenmesi",
                    Message = "Hizmet Güncellenemedi",
                    Css = "danger"
                });
            }
            return RedirectToAction("Service", "Admin");
        }

        [Authorize]
        public IActionResult ServiceDelete(int id)
        {
            Service entity = _serviceService.GetById(id);
            var result = _serviceService.Delete(entity);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hizmet Silme İşlemi",
                    Message = entity.Title + " Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Hizmet Silme İşlemi",
                    Message = entity.Title + " Silinmedi!",
                    Css = "danger"
                });
            }
            return RedirectToAction("Service", "Admin");
        }

        [Authorize]
        public IActionResult ServiceCreate()
        {
            ServiceModel model = new ServiceModel();
            model.SelectedService = new Service();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult ServiceCreate(ServiceModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
                result = _serviceService.Create(model.SelectedService);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedService.Title + "\" başlıklı kayıt eklendi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedService.Title + "\" başlıklı kayıt eklenemedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("Service", "Admin");
        }


        #endregion

        #region ContactUs


        [Authorize]
        public IActionResult Contact()
        {
            ContactInfoModel model = new ContactInfoModel();            
            model.ContactInfo = _contactInfoService.GetFirstContactInfo();

            if (model.ContactInfo == null)
                model.ContactInfo = new ContactInfo();

            return View(model);
        }
        [HttpPost]
        [Authorize]
        public IActionResult Contact(ContactInfoModel model)
        {
            bool result = false;
            if (ModelState.IsValid)
            {
                if (model.ContactInfo.Id > 0)
                    result = _contactInfoService.Update(model.ContactInfo);
                else
                    result = _contactInfoService.Create(model.ContactInfo);
            }
            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "İletişim Güncellenmesi",
                    Message = "İletişim Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "İletişim Güncellenmesi",
                    Message = "İletişim Güncellenemedi",
                    Css = "danger"
                });
            }
            return View();
        }


        #endregion

        #region Menu


        [Authorize]
        public IActionResult Menu()
        {
            MenuModel model = new MenuModel();
            model.Menus = _menuService.GetAll();
            return View(model);
        }

        [Authorize]
        public IActionResult MenuDetails(int id)
        {
            MenuModel model = new MenuModel();
            model.SelectedMenu = _menuService.GetById(id);
            return View(model);
        }

        [Authorize]
        public IActionResult MenuEdit(int id)
        {
            MenuModel model = new MenuModel();
            model.SelectedMenu = _menuService.GetById(id);
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> MenuEdit(MenuModel model, IFormFile file)
        {
            //Resim Eklenme işlemi
            if (file != null)
            {
                model.SelectedMenu.Image = file.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            bool result = false;
            if (ModelState.IsValid)
                result = _menuService.Update(model.SelectedMenu);

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedMenu.Title + "\" başlıklı Menü Güncellenmesi",
                    Message = "Menü Güncellendi",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "\"" + model.SelectedMenu.Title + "\" başlıklı Menü Güncellenmesi",
                    Message = "Menü Güncellenemedi",
                    Css = "danger"
                });
            }
            return RedirectToAction("Menu", "Admin");
        }

        [Authorize]
        public IActionResult MenuDelete(int id)
        {
            Menu entity = _menuService.GetById(id);
            var result = _menuService.Delete(entity);

            if (result)
            {
                //Resimi gidip yerinden siliyoruz
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", entity.Image);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
            }

            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Menü Silme İşlemi",
                    Message = entity.Title + " Silindi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Menü Silme İşlemi",
                    Message = entity.Title + " Silinmedi!",
                    Css = "danger"
                });
            }
            return RedirectToAction("Menu", "Admin");
        }

        [Authorize]
        public IActionResult MenuCreate()
        {
            MenuModel model = new MenuModel();
            model.SelectedMenu = new Menu();
            return View(model);
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> MenuCreate(MenuModel model, IFormFile file)
        {
            //Resim Eklenme işlemi
            if (file != null)
            {
                model.SelectedMenu.Image = file.FileName;
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", file.FileName);
                using(var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            bool result = false;
            if (ModelState.IsValid)
                result = _menuService.Create(model.SelectedMenu);
          
            if (result)
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedMenu.Title + "\" başlıklı kayıt eklendi.",
                    Css = "success"
                });
            }
            else
            {
                TempData.Put("message", new ResultMessage
                {
                    Title = "Yeni Kayıt Ekleme İşlemi",
                    Message = "\"" + model.SelectedMenu.Title + "\" başlıklı kayıt eklenemedi.",
                    Css = "danger"
                });
            }
            return RedirectToAction("Menu", "Admin");
        }


        #endregion

    }
}
