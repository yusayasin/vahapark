﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Models
{
    public class ResetPasswordModel
    {
        [Required]
        public string Token { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(6)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(6)]
        [Compare("Password", ErrorMessage ="Lutfen Şifreler aynı olsun")]
        public string RePassword { get; set; }
    }
}
