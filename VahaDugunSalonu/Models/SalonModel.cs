﻿using System.Collections.Generic;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Models
{
    public class SalonModel
    {
        public List<Salon> Salons { get; set; }

        public Salon SelectedSalon { get; set; }

        public List<SalonImage> SalonImages { get; set; }

        public SalonImage SelectedSalonImage { get; set; }
    }
}
