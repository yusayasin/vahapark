﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Models
{
    public class LoginModel
    {
        //Captcha
        [Required]
        [StringLength(4)]
        public string CaptchaCode { get; set; }

        [Required(ErrorMessage = "Lutfen Kullanıcı adı giriniz")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Lutfen Sifre giriniz")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool IsPersistent { get; set; }

        public string ReturnUrl { get; set; }
    }
}
