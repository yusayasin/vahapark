﻿using System.Collections.Generic;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Models
{
    public class MenuModel
    {
        public List<Menu> Menus { get; set; }
        public Menu SelectedMenu { get; set; }
    }
}
