﻿using System.Collections.Generic;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Models
{
    public class GalleryModel
    {
        public List<Gallery> Galleries { get; set; }
        public Gallery SelectedGallery { get; set; }
    }
}
