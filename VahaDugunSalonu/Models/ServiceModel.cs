﻿using System.Collections.Generic;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Models
{
    public class ServiceModel
    {
        public List<Service> Services { get; set; }

        public Service SelectedService { get; set; }
    }
}
