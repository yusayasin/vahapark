﻿using System.Collections.Generic;
using VahaDugunSalonu.Entities;

namespace VahaDugunSalonu.Models
{
    public class AboutUsModel
    {
        public List<AboutUs> AboutUss { get; set; }

        public AboutUs SelectedAboutUs { get; set; }
    }
}
