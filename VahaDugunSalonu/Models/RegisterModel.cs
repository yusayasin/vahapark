﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage ="Lutfen Ad soyad giriniz")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Lutfen Kullanıcı adı giriniz")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(6)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(6)]
        [Compare("Password", ErrorMessage ="Lutfen Şifreler aynı olsun")]
        public string RePassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
