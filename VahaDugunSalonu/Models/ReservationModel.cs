﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Models
{
    public class ReservationModel
    {
        //Captcha
        [Required]
        [StringLength(4)]
        public string CaptchaCode { get; set; }

        [Required(ErrorMessage = "Lutfen Rezervasyon tarihi giriniz.")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString ="{0:dd/MMM/yyyy HH:mm:ss}")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Lutfen Organizasyon seçiniz.")]
        public string OrganizationType { get; set; }

        [Required(ErrorMessage = "Kişi sayısı giriniz.")]
        [Range(1, int.MaxValue, ErrorMessage = "Lutfen sıfırdan büyük rakam giriniz.")]
        public int GuestNumber { get; set; }

        public string Not { get; set; }

        [Required(ErrorMessage = "Lutfen ad soyad giriniz.")]
        public string Fullname { get; set; }

        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Yanlış telefon değerleri girdiniz.")]
        [Required(ErrorMessage = "Lutfen telefon numarası giriniz.")]
        [StringLength(13, MinimumLength = 10, ErrorMessage ="Lutfen doğru telefon numarası giriniz.")]
        public string Phone { get; set; }

        [EmailAddress(ErrorMessage = "Lutfen doğru e-posta giriniz.")]
        public string Email { get; set; }

    }
}
