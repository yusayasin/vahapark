﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using VahaDugunSalonu.Identity;

namespace VahaDugunSalonu.Models
{
    public class UserListModel
    {
        public List<ApplicationUser> Users { get; set; }
        public UserManager<ApplicationUser> UserManager { get; set; }
    }
}
