﻿using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using VahaDugunSalonu.Identity;
using VahaDugunSalonu.Models;

namespace VahaDugunSalonu.Help
{
    public class SendEmail
    {
        private IConfiguration _config;
        //private string _url = "https://localhost:44317";
        private string _apiKey = "";// "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";//System.Environment.GetEnvironmentVariable("VahaParkAppEmailService");

        private string _from = "";//"yusayasin@hotmail.com";
        private string _to = ""; //"yusayasin@gmail.com";
        private string _url = "";

        public SendEmail(IConfiguration config)
        {
            _config = config;
            _apiKey = _config["Data:Email:ApiKeyCode"];
            _from = _config["Data:Email:From"];
            _to = _config["Data:Email:To"];
            _url = _config["Url"];
        }

        /// <summary>
        /// Sending ConfirmEmail for user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="callbackUrl"></param>
        /// <returns></returns>
        public async Task SendConfirmEmail(ApplicationUser user, string callbackUrl)
        {
            string _url = _config["Url"];
            var client = new SendGridClient(_apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(_from, "Vaha Park"),
                Subject = "Onay Mesajı",
                PlainTextContent = "Hello, Email!",
                HtmlContent = $"<strong>Sayın {user.FullName}</strong> Lutfen email hesabınızı onaylamak için linke <a href='{_url}{callbackUrl}'>tıklayınız.</a><Br>" +
                              $"Eğer çalışmaz ise aşağıdaki linki kullanın <Br>{_url}{callbackUrl}"
            };
            msg.AddTo(new EmailAddress(user.Email, "Test User"));
            var response = await client.SendEmailAsync(msg);
        }

        /// <summary>
        /// Sending ResetPasswordEmail for user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="callbackUrl"></param>
        /// <returns></returns>
        public async Task SendEmailToResetPassword(ApplicationUser user, string callbackUrl)
        { 
            var client = new SendGridClient(_apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(_from, "Vaha Park"),
                Subject = "Vaha Park Şifre Yenileme",
                PlainTextContent = "Hello, Email!",
                HtmlContent = $"<strong>Sayın {user.FullName}</strong> Lutfen Şifre yenilemek için linke <a href='{_url}{callbackUrl}'>tıklayınız.</a><Br>" +
                              $"Eğer çalışmaz ise aşağıdaki linki kullanın <Br>{_url}{callbackUrl}"
            };
            msg.AddTo(new EmailAddress(user.Email, "Test User"));
            var response = await client.SendEmailAsync(msg);
        }

        /// <summary>
        /// Sending Reservation Request for customer
        /// </summary>
        /// <param name="user"></param>
        /// <param name="callbackUrl"></param>
        /// <returns></returns>
        public async Task SendReservationEmail(ReservationModel model)
        {
            var client = new SendGridClient(_apiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(_from, "Vaha Park Rezervasyon"),
                Subject = "Vaha Park Rezervasyon",
                PlainTextContent = "Hello, Email!",
                HtmlContent = $"<p><strong>" + model.Date + $"</strong> tarihi i&ccedil;in&nbsp;tahmini&nbsp;<strong>" + model.GuestNumber + $"</strong>" +
                              $"&nbsp;kişilik&nbsp;<strong>" + model.OrganizationType + $"&nbsp;</strong>d&uuml;ş&uuml;nmekteyim.</p>" +
                              $"<p><strong>Not</strong>: " + model.Not + $"</p>" +
                              $"<p><strong>" + model.Fullname + $"&nbsp;</strong></p>" +
                              $"<p><strong>Telefon:</strong>" + model.Phone + $"</p>" +
                              (string.IsNullOrWhiteSpace(model.Email) ? "" : $"<p><strong>E-Posta:</strong>Email</p>")
            };
            msg.AddTo(new EmailAddress(_to, "Cengiz"));
            var response = await client.SendEmailAsync(msg);
        }
    }
}
