using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI;
using SendGrid;
using SendGrid.Helpers.Mail;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using VahaDugunSalonu.Bussiness.Abstract;
using VahaDugunSalonu.Bussiness.Concrete;
using VahaDugunSalonu.DataAccess.Abstract;
using VahaDugunSalonu.DataAccess.Concrete.EFCore;
using VahaDugunSalonu.DataAccess.Concrete.Memory;
using VahaDugunSalonu.Identity;
using VahaDugunSalonu.Models;
using Edi.Captcha;

namespace VahaDugunSalonu
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ////Captcha & Session
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(10);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            //services.AddDbContext<VahaDugunSalonuContext>(options =>
            //options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<ApplicationIdentityDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("IdentityConnection")));


            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;

                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;

                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(1);
                options.Lockout.AllowedForNewUsers = true;
                options.SignIn.RequireConfirmedEmail = true;

            });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/admin/LogIn";
                options.LogoutPath = "/admin/LogOut";
                options.AccessDeniedPath = "/admin/AccessDenied";
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.Cookie = new CookieBuilder
                {
                    HttpOnly = true,
                    Name = ".VahaPark.Security.Cookie",
                    SameSite = SameSiteMode.Strict
                };
            });

            services.AddScoped<IGalleryDal, EFCoreGalleryDal>();//MemoryGalleryDal
            services.AddScoped<IGalleryService, GalleryManager>();

            services.AddScoped<IContactInfoDal, EFCoreContactInfoDal>();//MemoryContactInfoDal
            services.AddScoped<IContactInfoService, ContactInfoManager>();

            services.AddScoped<IAboutUsDal, EFCoreAboutUsDal>();//MemoryAboutUsDal
            services.AddScoped<IAboutUsService, AboutUsManager>();

            services.AddScoped<ISalonDal, EFCoreSalonDal>();//MemorySalonDal
            services.AddScoped<ISalonService, SalonManager>();

            services.AddScoped<IServiceDal, EFCoreServiceDal>();//MemoryServiceDal
            services.AddScoped<IServiceService, ServiceManager>();

            services.AddScoped<IMenuDal, EFCoreMenuDal>();//MemoryMenuDal
            services.AddScoped<IMenuService, MenuManager>();

            services.AddScoped<ISalonImageDal, EFCoreSalonImageDal>();//MemorySalonImageDal
            services.AddScoped<ISalonImageService, SalonImageManager>();


            //Captcha
            services.AddSessionBasedCaptcha();
            //Captcha
            services.AddSessionBasedCaptcha(option =>
            {
                option.Letters = "2346789ABCDEFGHJKLMNPRTUVWXYZ";
                option.SessionName = "CaptchaCode";
                option.CodeLength = 4;
            });
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //Captcha
            app.UseSession();

            //Captcha
            app.UseSession().UseCaptchaImage(options =>
            {
                options.RequestPath = "/captcha-image";
                options.ImageHeight = 36;
                options.ImageWidth = 100;
            });
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            //SeedIdentity.Seed(userManager, roleManager, Configuration).Wait();
        }
    }
}
