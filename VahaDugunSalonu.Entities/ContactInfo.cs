﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Entities
{
    public class ContactInfo
    {
        public int Id { get; set; }
        public string Explanation { get; set; }

        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Yanlış telefon değerleri girdiniz.")]
        [Required(ErrorMessage = "Lutfen telefon numarası giriniz.")]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Lutfen doğru telefon numarası giriniz.")]
        public string Phone { get; set; }

        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Yanlış telefon değerleri girdiniz.")]
        [Required(ErrorMessage = "Lutfen telefon numarası giriniz.")]
        [StringLength(13, MinimumLength = 10, ErrorMessage = "Lutfen doğru telefon numarası giriniz.")]
        public string Phone2 { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(1000)]
        public string GoogleMap { get; set; }

        [StringLength(11)]
        public string WorkTime { get; set; }

        [StringLength(60)]
        [EmailAddress]
        public string Email { get; set; }


    }
}
