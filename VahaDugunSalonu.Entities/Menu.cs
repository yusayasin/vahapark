﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Entities
{
    public class Menu
    {
        public int Id { get; set; }

        [StringLength(150)]
        public string Image { get; set; }

        [StringLength(150)]
        public string Title { get; set; }
        public string Paragraph { get; set; }
        public bool IsEnable { get; set; }

    }
}
