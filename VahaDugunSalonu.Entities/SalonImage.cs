﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Entities
{
    public class SalonImage
    {
        public int Id { get; set; }

        [StringLength(150)]
        public string Image { get; set; }

        public int SalonId { get; set; }

        public bool IsEnable { get; set; }
    }
}
