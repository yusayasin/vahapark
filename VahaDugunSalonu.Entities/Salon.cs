﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Entities
{
    public class Salon
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Icon { get; set; }

        [StringLength(150)]
        public string Title { get; set; }
        public string Paragraph { get; set; }
        public bool IsEnable { get; set; }
    }
}
