﻿using System.ComponentModel.DataAnnotations;

namespace VahaDugunSalonu.Entities
{
    public class Gallery
    {
        public int Id { get; set; }

        [StringLength(150)]
        public string ImageName { get; set; }

        [StringLength(150)]
        public string Alt { get; set; }

        [StringLength(150)]
        public string Title { get; set; }
        public string Paragraph { get; set; }
        public bool IsEnable { get; set; }
    }
}
